function initBalls() {
  balls = [];

        var blue = '#3A5BCD';
        var red = '#EF2B36';
        var yellow = '#FFC636';
        var purple = '#7D26CD';
        var cyan = '#00CDCD';
        var orange = '#FFA500';
        var black = '#000000';

  // F
  balls.push(new Ball(110, 70, 0, 0, blue));
  balls.push(new Ball(100, 70, 0, 0, blue));
  balls.push(new Ball(90, 70, 0, 0, blue));
  balls.push(new Ball(80, 70, 0, 0, blue));
  balls.push(new Ball(70, 70, 0, 0, blue));
  balls.push(new Ball(70, 80, 0, 0, blue));
  balls.push(new Ball(70, 90, 0, 0, blue));
  balls.push(new Ball(80, 100, 0, 0, blue));
  balls.push(new Ball(90, 100, 0, 0, blue));
balls.push(new Ball(70, 100, 0, 0, blue));
balls.push(new Ball(70, 110, 0, 0, blue));  
balls.push(new Ball(70, 120, 0, 0, blue));
balls.push(new Ball(70, 130, 0, 0, blue));
balls.push(new Ball(70, 140, 0, 0, blue));

//o
balls.push(new Ball(140, 100, 0, 0, red));
balls.push(new Ball(150, 100, 0, 0, red));
balls.push(new Ball(160, 100, 0, 0, red));
balls.push(new Ball(170, 110, 0, 0, red));
balls.push(new Ball(170, 120, 0, 0, red));
balls.push(new Ball(170, 130, 0, 0, red));
balls.push(new Ball(160, 140, 0, 0, red));
balls.push(new Ball(150, 140, 0, 0, red));
balls.push(new Ball(140, 140, 0, 0, red));
balls.push(new Ball(130, 130, 0, 0, red));
balls.push(new Ball(130, 110, 0, 0, red));
balls.push(new Ball(130, 120, 0, 0, red));

//r
balls.push(new Ball(200, 80, 0, 0, yellow));
balls.push(new Ball(200, 90, 0, 0, yellow));
balls.push(new Ball(200, 100, 0, 0, yellow));
balls.push(new Ball(200, 110, 0, 0, yellow));
balls.push(new Ball(200, 120, 0, 0, yellow));
balls.push(new Ball(200, 130, 0, 0, yellow));
balls.push(new Ball(200, 140, 0, 0, yellow));
balls.push(new Ball(220, 80, 0, 0, yellow));
balls.push(new Ball(230, 80, 0, 0, yellow));
balls.push(new Ball(210, 90, 0, 0, yellow));
balls.push(new Ball(230, 80, 0, 0, yellow));

//ó
balls.push(new Ball(260, 140, 0, 0, purple));
balls.push(new Ball(270, 140, 0, 0, purple));
balls.push(new Ball(280, 140, 0, 0, purple));
balls.push(new Ball(290, 130, 0, 0, purple));
balls.push(new Ball(290, 120, 0, 0, purple));
balls.push(new Ball(290, 110, 0, 0, purple));
balls.push(new Ball(280, 100, 0, 0, purple));
balls.push(new Ball(270, 100, 0, 0, purple));
balls.push(new Ball(260, 100, 0, 0, purple));
balls.push(new Ball(250, 130, 0, 0, purple));
balls.push(new Ball(250, 120, 0, 0, purple));
balls.push(new Ball(250, 110, 0, 0, purple));
balls.push(new Ball(290, 80, 0, 0, purple));1
balls.push(new Ball(280, 80, 0, 0, purple));

//I
balls.push(new Ball(310, 140, 0, 0, cyan));
balls.push(new Ball(320, 140, 0, 0, cyan));
balls.push(new Ball(330, 140, 0, 0, cyan));
balls.push(new Ball(340, 140, 0, 0, cyan));
balls.push(new Ball(350, 140, 0, 0, cyan));
balls.push(new Ball(330, 130, 0, 0, cyan));
balls.push(new Ball(330, 120, 0, 0, cyan));
balls.push(new Ball(330, 110, 0, 0, cyan));
balls.push(new Ball(330, 100, 0, 0, cyan));
balls.push(new Ball(340, 100, 0, 0, cyan));
balls.push(new Ball(350, 100, 0, 0, cyan));
balls.push(new Ball(320, 100, 0, 0, cyan));
balls.push(new Ball(310, 100, 0, 0, cyan));

//g
balls.push(new Ball(410, 90, 0, 0, orange));
balls.push(new Ball(400, 90, 0, 0, orange));
balls.push(new Ball(390, 90, 0, 0, orange));
balls.push(new Ball(380, 100, 0, 0, orange));
balls.push(new Ball(380, 110, 0, 0, orange));
balls.push(new Ball(380, 120, 0, 0, orange));
balls.push(new Ball(380, 130, 0, 0, orange));
balls.push(new Ball(390, 130, 0, 0, orange));
balls.push(new Ball(400, 130, 0, 0, orange));
balls.push(new Ball(410, 130, 0, 0, orange));
balls.push(new Ball(410, 120, 0, 0, orange));
balls.push(new Ball(410, 110, 0, 0, orange));
balls.push(new Ball(410, 100, 0, 0, orange));
balls.push(new Ball(410, 140, 0, 0, orange));
balls.push(new Ball(410, 150, 0, 0, orange));
balls.push(new Ball(410, 160, 0, 0, orange));
balls.push(new Ball(400, 160, 0, 0, orange));
balls.push(new Ball(380, 160, 0, 0, orange));
balls.push(new Ball(390, 160, 0, 0, orange));

//E
balls.push(new Ball(470, 140, 0, 0, black));
balls.push(new Ball(460, 140, 0, 0, black));
balls.push(new Ball(450, 140, 0, 0, black));
balls.push(new Ball(440, 140, 0, 0, black));
balls.push(new Ball(440, 130, 0, 0, black));
balls.push(new Ball(440, 120, 0, 0, black));
balls.push(new Ball(440, 110, 0, 0, black));
balls.push(new Ball(440, 100, 0, 0, black));
balls.push(new Ball(450, 100, 0, 0, black));
balls.push(new Ball(460, 100, 0, 0, black));
balls.push(new Ball(470, 100, 0, 0, black));
balls.push(new Ball(460, 120, 0, 0, black));
balls.push(new Ball(450, 120, 0, 0, black));

    return balls;
}

